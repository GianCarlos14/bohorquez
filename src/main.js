import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import Carousel from 'bulma-carousel'
import { VueExtendLayout, layout } from 'vue-extend-layout'

document.addEventListener('DOMContentLoaded', function(){
  var carousels = Carousel.attach();
}, false);

Vue.config.productionTip = false;
Vue.use(VueExtendLayout);

import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyC_cu2WjYNq2nAmcfHaqEspSHgFy2JTL9g',
  },
});

new Vue({
  router,
  store,
  render: h => h(App),
  ...layout
}).$mount('#app');
